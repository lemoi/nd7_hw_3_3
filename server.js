const express = require('express');
const bodyParser = require('body-parser');
const app = express();

const mongoose = require('mongoose');
mongoose.Promise = global.Promise; // для работы с Promise

const urlDB = 'mongodb://localhost:27017/nd7_hw_3_3';
const Schema = mongoose.Schema;
const ObjectId = mongoose.Types.ObjectId;

mongoose.connect(urlDB, { useMongoClient: true });

let userScheme = new Schema({
    name: String
});

let taskScheme = new Schema({
    name: String,
    description: String,
    complete: Boolean,
    user: mongoose.Schema.Types.ObjectId,
    dateComplete: Date
})

let User = mongoose.model('User', userScheme);
let Task = mongoose.model('Task', taskScheme);

getUser({})
    .then(user => {
        if (user !== null) {
            return; // заполнять ничего не нужно
        }

        // иначе подготовим массив пользовательей для создания
        let users = [
            {name: 'Иванов'}, {name: 'Петров'}, {name: 'Сидоров'}, {name: 'Федоров'}
        ];
        // Заполним базу тестовыми данными
        return User.create(users);
    })
    .then(users => {
        if (!users) {
            return;
        }

        // Подготовим и сохраним список задач
        let descriptions = ['сбор требований', 'разработка', 'отладка', 'тестирование', 'руководство'];
        let tasks = [];
        for (let i = 1; i < users.length * getRandomInt(25, 200); i++) {
            let task = {};
            task.name = 'task' + i;
            task.description = descriptions[getRandomInt(0, descriptions.length - 1)];
            if (Math.random() > 0.5) {
                // Только часть задач уже поручена (50%)
                task.user = users[getRandomInt(0, users.length - 1)]._doc._id;
            }
            if ('user' in task) {
                // Из порученных задач выполнена только 30%
                task.complete = Math.random() > 0.7;
            }
            if (task.complete) {
                // Если задача завершена должна быть дата завершения
                task.dateComplete = (new Date).setSeconds(-getRandomInt(15 * 86400, 600 * 86400)); // переиод выполнения задач сдвигаем назад в интервал от 15 до 600 дней
            }
            tasks.push(task);
        }

        return Task.create(tasks);
    })
    .then(result => {
        if (!result) {
            return;
        }
        console.log('Создано', result.length, 'задач');
    });

// Добавить заполнение тестовыми данными если база пустая

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({"extended": true}));

const rpcAPI = express.Router();

let RPC = {
    getUsers: function(params) {
        return getUsers(params)
            .then(users => {
                return users;
            });
    },

    getUser: function(params) {
        return getUser(params)
            .then(user => {
                return user;
            });
    },

    createUser: function(params) {
        return createUser(params)
            .then(user => {
                return user;
            })
    },

    updateUser: function(params) {
        return updateUser(params.find, params.update)
            .then(user => {
                return user;
            })
    },

    deleteUser: function(params) {
        return deleteUser(params)
            .then(result => {
                return result;
            })
    },

    deleteUsers: function(params) {
        return deleteUsers(params)
            .then(result => {
                return result;
            })
    },

    getTasks: function(params) {
        return getTasks(params)
            .then(tasks => {
                return tasks;
            });
    },

    getTask: function(params) {
        return getTask(params)
            .then(task => {
                return task;
            });
    },

    createTask: function(params) {
        return createTask(params)
            .then(task => {
                return task;
            })
    },

    updateTask: function(params) {
        return updateTask(params.find, params.update)
            .then(task => {
                return task;
            })
    },

    deleteTask: function(params) {
        return deleteTask(params)
            .then(result => {
                return result;
            })
    },

    deleteTasks: function(params) {
        return deleteTasks(params)
            .then(result => {
                return result;
            })
    },

    executeTask: function(params) {
        return executeTask(params.task, params.complete === undefined ? true : params.complete)
            .then(result => {
                return result;
            })
    },

    delegateTask: function(params) {
        return delegateTask(params.task, params.user)
            .then(result => {
                return result;
            })
    },

    getUsersTaskComplete: function(params) {
        return getUsersTaskComplete(params)
            .then(result => {
                return result;
            })
    }
}

rpcAPI.post("/rpc", function(req, res) {
    const method = RPC[req.body.method];
    if(typeof method != "function") {
        console.log('Метод', req.body.method, 'не определен');
        console.log('Параметры:', req.body.params);
        return res.status(400).send();
    };
    try {
        method(req.body.params)
            .then(result => {
                return res.json(result);
            });
    } catch(error) {
        console.error(error);
        return res.status(500).send();
    }
});

app.use("/api/v1", rpcAPI);

app.listen(3000, () => { console.log("Ожидаем подключения")});

function getUsers(params) {
    return User.find(params);
}

function getUser(params) {
    return User.findOne(params);
}

function createUser(params) {
    return User.create(params);
}

function updateUser(find, update) {
    let options = {
        'new': true, // возвращаем измененный документ
        'upsert': true // создаем новый, если не нашли
    }
    return User.findOneAndUpdate(find, update, options);
}

function deleteUser(params) {
    return User.deleteOne(params);
}

function deleteUsers(params) {
    return User.deleteMany(params);
}


function getTasks(params) {
    return Task.find(configUser(params), null, {sort: {dateComplete: -1}}); // вначале будут последние завершенные задачи
}

function getTask(params) {
    return Task.findOne(configUser(params));
}

function createTask(params) {
    return Task.create(configUser(params));
}

function updateTask(find, update) {
    let options = {
        'new': true, // возвращаем измененный документ
        'upsert': true // создаем новый, если не нашли
    }
    return Task.findOneAndUpdate(configUser(find), configUser(update), options);
}

function deleteTask(params) {
    return Task.deleteOne(configUser(params));
}

function deleteTasks(params) {
    return Task.deleteMany(configUser(params));
}

function executeTask(taskID, complete) {
    return Task.findByIdAndUpdate(new ObjectId(taskID), {$set: {complete: complete}}, {new: true});
}

function delegateTask(taskID, userID) {
    return Task.findByIdAndUpdate(new ObjectId(taskID), {$set: {user: new ObjectId(userID)}}, {new: true});
}


function getUsersTaskComplete(params) {
    return Task.aggregate([
        {$match: {complete: true} },
        {$project: {_id:1, user:1, count: {$add: 1}} },
        {$group: {_id: "$user", count: {$sum: "$count"}} },
        {$lookup: {
            from: "users",
            localField: "_id",
            foreignField: "_id",
            as: "user_docs"}
        },
        {$project: {userName: "$user_docs.name", count: "$count"}},
        {$unwind: "$userName" },
        {$sort: {count: -1} }
    ]);
}

function configUser(params) {
    newParams = cloneObject(params);
    if ('user' in newParams) {
        newParams.user = new ObjectId(newParams.user);
    }
    return newParams;
}
function cloneObject(obj) {
    return JSON.parse(JSON.stringify(obj));
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
